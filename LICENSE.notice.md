The Linux Kernel downloaded with these scripts is licensed under
the GNU GPL version 2.0. See KERNEL.md for instructions on obtaining its
source code.

The binary in `ramdisk/sbin/busybox` is the official BusyBox armv7 binary.
BusyBox is licensed under the GNU GPL version 2.0. See <https://busybox.net/>
for more information. Source code can be downloaded from 
<https://busybox.net/downloads/busybox-1.31.0.tar.bz2>.

The binary files `ramdisk/lib/firmware/gc*.bin` are the initialization commands sent
to the display panel on startup. The commands have been taken from the downstream
device tree, which is licensed under the GNU GPL version 2.0 and may only be
reproduced under GPL 2.0.
However, these binary files are so small that it does not make much sense to apply
a license to them. (I am not a lawyer, that's just my opinion.)

The GNU GPL version 3 only applies to the files with a notice that
they are licensed under the GNU GPL version 3, such as the Makefile, some of
the installer scripts, and the shell scripts in the `ramdisk` directory.
