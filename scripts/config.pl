#!/usr/bin/env perl
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
my $host = "bananaphone", $device = "argon", $dist = "unstable", $extra = "vim";

if (open my $cfgfile, "<", ".config") {
  while (<$cfgfile>) {
    if (/^CONFIG_HOSTNAME="(.*)"/) {
      $host = $1;
    }
    elsif (/^CONFIG_DEVICE="(.*)"/) {
      $device = $1;
    }
    elsif (/^CONFIG_DIST="(.*)"/) {
      $dist = $1;
    }
    elsif (/^CONFIG_EXTRA="(.*)"/) {
      $extra = $1;
    }
  }
  close $cfgfile;
}

open my $cfgfile, ">", ".config" or die("Failed to open config file");

print "Device hostname [$host]: ";
$_ = <STDIN>;
if (/^(.+)$/) {
  $host = $1;
}

my $devicepkgfile;
do {
  print "Available devices:\n";
  while (<'devices/*/description'>) {
    my $filename = $_;
    open(my $file, $filename) or die("Open $filename failed");
    if (/^devices\/(.*)\/description$/) {
      print " - $1: ";
    }
    while (<$file>) {
      print $_;
    }
    close $file;
  }

  print "Device type [$device]: ";
  $_ = <STDIN>;
  if (/^(.+)$/) {
    $device = $1;
  }
} while not open($devicepkgfile, "devices/$device/packages");

my $upower = 'upower';
my @devicepkglist;
while (<$devicepkgfile>) {
  s/\n//;
  if ($_ eq 'bpowerd') {
    # Don't install UPower if bpowerd is used
    $upower = '';
  }
  push @devicepkglist, $_;
}
my $devicepkgs = join ' ', @devicepkglist;
close $devicepkgfile;

do {
  print "Update channel (unstable/stable/archive) [$dist]: ";
  $_ = <STDIN>;
  if (/^(.+)$/) {
    $dist = $1;
  }
} while ($dist ne "stable" and $dist ne "unstable" and $dist ne "archive");

print "Extra packages (space-separated) [$extra]: ";
$_ = <STDIN>;
if (/^(.+)$/) {
  $extra = $1;
}

print $cfgfile "CONFIG_HOSTNAME=\"$host\"\n";
print $cfgfile "CONFIG_DEVICE=\"$device\"\n";
print $cfgfile "CONFIG_DIST=\"$dist\"\n";
print $cfgfile "CONFIG_EXTRA=\"$extra\"\n";
close $cfgfile;
open my $cfgfile, ">", "multistrap.ini" or die("Failed to open multistrap.ini");
print $cfgfile <<END;
[General]
arch=armhf
cleanup=false
noauth=false
unpack=true
explicitsuite=false
multiarch=
aptsources=Debian Bananian
bootstrap=Debian Bananian
addimportant=true

[Debian]
packages=hicolor-icon-theme adwaita-icon-theme openssh-server connman wpasupplicant man-db busybox sudo pulseaudio libpam-systemd sakura dbus-user-session fonts-open-sans fonts-noto-color-emoji dnsmasq-base iptables locales-all usrmerge polkitd $extra $upower
source=http://deb.debian.org/debian
keyring=debian-archive-keyring
suite=bookworm

[Bananian]
packages=wbananui $devicepkgs
source=http://89.58.28.240/bananian
keyring=bananian-archive-keyring
suite=$dist
END

print "Execute make to start the build.\n";
