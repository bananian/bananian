# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
gitlab_kernel () {
  if [ ! -f dl/.zImage.stamp ]; then
    mkdir -p dl
    rm -rf dl/zImage*
    wget -Odl/zImage.zip "$1/-/jobs/artifacts/$2/download?job=build" || exit 1
    unzip dl/zImage.zip -d dl/zImage-files
    touch dl/.zImage.stamp
  fi
}
extract_kernel () {
  if [ -z "$2" ]; then
    cp "dl/zImage-files/$1" zImage
  else
    # append DTB
    cat "dl/zImage-files/$1" "dl/zImage-files/$2" > zImage
  fi
  rm -rf zImage-modules
  cp -rf dl/zImage-files/tmp_modules/lib/modules zImage-modules
}
gitlab_lk2nd () {
  if [ ! -f dl/.lk2nd.stamp ]; then
    mkdir -p dl
    rm -rf dl/lk2nd*
    wget -Odl/lk2nd.zip "$1/-/jobs/artifacts/$2/download?job=build" || exit 1
    unzip dl/lk2nd.zip -d dl/lk2nd-files
    mv dl/lk2nd-files/build-lk2nd-*/lk2nd.img dl/lk2nd.img
    rm -rf dl/lk2nd-files
    touch dl/.lk2nd.stamp
  fi
}
generate_lk2nd_image () {
  cp dl/lk2nd.img boot.img
  dd if="$1" of=boot.img bs=1024 seek=512 conv=notrunc
}
