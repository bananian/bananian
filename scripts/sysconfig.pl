#!/usr/bin/env perl
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

open(my $cfg, '.config') or die ('Open config failed');
my $host;

while (<$cfg>) {
  if (/^CONFIG_HOSTNAME="(.*)"$/) {
    $host = $1;
  }
}

die ('No hostname configured, please re-run make config') unless $host;

my $fh;

open $fh, ">", "$ENV{DEBROOT}/etc/hostname"
  or die ("open hostname output failed");
print $fh "$host\n";
