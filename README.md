# Bananian - Debian for the Nokia 8110 4G

Important resources:
 - [Matrix space](https://matrix.to/#/#bananian-sfp-os:matrix.org)
 - [APT repository](http://89.58.28.240/bananian/)
 - [User guide (wiki)](https://git.abscue.de/bananian/bananian/-/wikis/home)

## What is Bananian
Bananian is a custom installation of [Debian](https://debian.org) for the
Nokia 8110 4G, the Nokia 6300 4G, the Nokia 8000 4G and similar phones.
This repository provides some scripts to install the
system onto your phone.

## What Bananian is not
Bananian is not a usable replacement for [KaiOS](https://www.kaiostech.com) yet.
However, it
might become one in the future once it gets more supported features.

## Installing
This step requires a **rooted** phone. See
[BananaHackers](https://sites.google.com/view/bananahackers/root) for more info.

#### Requirements

 - download the image or [build one yourself](#building)
   - [8110 (argon) stable download](https://git.abscue.de/bananian/bananian/-/jobs/artifacts/devel/download?job=build-defconfig-argon-stable)
   - [8110 (argon) unstable download](https://git.abscue.de/bananian/bananian/-/jobs/artifacts/devel/download?job=build-defconfig-argon-unstable)
   - [8000 (sparkler) unstable download](https://git.abscue.de/bananian/bananian/-/jobs/artifacts/devel/download?job=build-defconfig-sparkler-unstable)
   - [6300 (leo) unstable download](https://git.abscue.de/bananian/bananian/-/jobs/artifacts/devel/download?job=build-defconfig-leo-unstable)
 - `adb` must be installed.

If you downloaded the image, unzip it and run `unxz debroot.tar.xz`.

**NOTE:** For Mainline distributions (currently all unstable distributions),
you can mount the SD card and unpack debroot.tar on your computer directly. If
you are using the 3.10 downstream kernel (supplied with argon-stable), you should
not mount the SD card on your computer.

Create two partitions on your SD card. Format the first one as FAT and the
second as EXT4. (Extract the system now if you are using mainline.)
Power off the phone and insert the SD card.

If you are using mainline, back up your KaiOS boot partition and flash _it_ onto
`recovery`. Then flash your new Bananian boot image onto the `boot` partition.

If you are using downstream, power up your phone, enable root access if needed,
and run the following commands on your computer with the phone plugged in:

```
$ adb shell mkdir -p /data/debian
$ adb shell busybox mount /dev/block/mmcblk1p2 /data/debian
$ adb push debroot.tar /data/debian
$ adb push boot.img /sdcard/bananian.img
$ adb shell
root@Nokia 8110 4G:/# cd /data/debian
root@Nokia 8110 4G:/data/debian# busybox tar xvf debroot.tar
root@Nokia 8110 4G:/data/debian# rm debroot.tar
root@Nokia 8110 4G:/data/debian# cd /
root@Nokia 8110 4G:/# umount /data/debian
root@Nokia 8110 4G:/# dd if=/sdcard/bananian.img of=/dev/block/bootdevice/by-name/recovery bs=2048
root@Nokia 8110 4G:/# reboot recovery
```


## Multiboot
Since Bananian is not a fully-featured system yet, it is meant to be
**multi-booted** (installed side-by-side with KaiOS). The root filesystem is
located on the SD card.

When using the mainline kernel, Bananian boots from the default `boot` partition
and KaiOS can be booted from the `recovery` partition.

If your are using downstream and want to put Bananian into the recovery partition,
but your stock system replaces recovery on every boot, you should disable that
or use a script like this to boot Bananian (put it somewhere into `/data`):

```sh
#!/system/bin/sh
set -e
dd if=/data/bananian.img of=/dev/block/bootdevice/by-name/recovery bs=2048
reboot recovery
```

#### First boot
On first boot, the phone has to complete the installation by running a
bootstrap process which configures the system packages and a "setup" process
which sets up the main user account. It also copies firmware from the stock system,
so you **shouldn't erase KaiOS** completely before the installation has completed.

**The bootstrap process can take more than 20 minutes.**

If an error occurs, the phone will vibrate and blink several times on downstream, or
boot to an error screen / login prompt on mainline.
Note that it won't reboot unless something crashes the system.
Press and hold the Power button
and the D-Pad "down" key (8110) or `#` key (8000) to restart the device if it hangs.

### Using it
See the [Wiki](https://git.abscue.de/bananian/bananian/-/wikis/home).

### Building
> **NOTE: This has been tested only on Debian and Devuan GNU/Linux. Building on
> Debian-based
> distributions such as Ubuntu might work, but this will certainly not work on
> other operating systems.**

Prepare the build system (as root):
```
# dpkg --add-architecture armhf
# echo "deb [arch=armhf] http://89.58.28.240/bananian unstable main" > /etc/apt/sources.list.d/bananian.list
# wget -O/etc/apt/trusted.gpg.d/bananian-archive.gpg https://storage.abscue.de/bananian/bananian-archive.gpg
# apt update
# apt install multistrap fakeroot mkbootimg
```

Then run `fakeroot make` (or `make` as root). The build should produce
`debroot.tar` and `boot.img`.

### Bugs
Please sign up and report an issue if you find a bug. If it's a UI bug,
please report it in the <https://git.abscue.de/bananian/wbananui> repository.

### Contribute
If you would like to contribute, sign up at https://git.abscue.de, make an issue
describing what you want to do, request access using the link at the bottom of the
page, and make a fork and a merge request.

Feel free to ask questions in Bananian's Matrix space.

If you need help developing an app, please ask in the **App development**
section of the Matrix chat.

### Which kernel does Bananian use?
See [KERNEL.md](KERNEL.md)

### Disclaimer
Install this ***AT YOUR OWN RISK***! I am not responsible for bricked phones or
missed phone calls!!!

This installer is distributed in the hope that it will be useful,
but **WITHOUT ANY WARRANTY**; without even the implied warranty of
**MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
GNU General Public License for more details.

Packages included in this distribution have different licenses.
Please see the [LICENSE.notice.md](./LICENSE.notice.md) file,
<https://www.debian.org/legal/licenses/> and the copyright files of the
packages (usually found at `/usr/share/doc/<package>/copyright` on the
installed system) more information
