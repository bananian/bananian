# Kernel sources

 - Nokia 8110 downstream: <https://gitlab.com/bananian/kernel-nokia-8110>
 - Nokia 8110/8000 mainline: <https://gitlab.com/bananian/msm8909-mainline>
   - Kernel config: <https://storage.abscue.de/private/zImage/msm8909-mainline-config>
