# Copyright (C) 2020-2022 Affe Null <affenull2345@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Makefile for bananian (installer)

DEBROOT = debroot

export DEBROOT

ifeq ($(wildcard .config),)

.PHONY: all
all: config

else

.PHONY: all
all: initrd.img boot.img $(DEBROOT)/UNPACKED_STAMP debroot.tar

endif

.config: scripts/config.pl
	@scripts/config.pl

multistrap.ini: .config

.PHONY: config
config:
	@scripts/config.pl

help:
	@echo 'Cleaning targets:'
	@echo '  clean              - Remove packages, root and other generated'
	@echo '                       files, excluding config and downloads'
	@echo '  distclean          - Remove almost everything'
	@echo ''
	@echo 'Configuration targets:'
	@echo '  config             - Update current config utilising a line-oriented program'

initrd.img: ramdisk
	rm -f $@
	scripts/pack-initrd $@ $<

zImage:
	@scripts/device-script.pl get-kernel

.PHONY: kernel-update
kernel-update: zImage

boot.img: initrd.img zImage
	@scripts/device-script.pl make-image

$(DEBROOT)/UNPACKED_STAMP: multistrap.ini
	rm -rf $(DEBROOT)
	multistrap -d $(DEBROOT) -f $< || (rm -rf $@; false)
	touch $@

.PHONY: copy-files
copy-files: $(DEBROOT)/UNPACKED_STAMP zImage
	rm -rf $(DEBROOT)/lib/modules
	cp -rf zImage-modules $(DEBROOT)/lib/modules
	mkdir -p $(DEBROOT)/var/lib/connman
	cp -f rndis_usb.config $(DEBROOT)/var/lib/connman
	chmod 600 $(DEBROOT)/var/lib/connman/rndis_usb.config
	@scripts/device-script.pl copy-files

.PHONY: sysconfig
sysconfig: $(DEBROOT)/UNPACKED_STAMP
	@scripts/sysconfig.pl

debroot.tar: $(DEBROOT)/UNPACKED_STAMP copy-files sysconfig
	rm -rf $@ $(DEBROOT)/etc/ssh/ssh_host_* $(DEBROOT)/var/cache/apt/archives/*
	@(echo '==>> Creating debroot.tar...' && cd $(DEBROOT) && \
	  tar cf $(CURDIR)/$@ --exclude=.gitignore *)

.PHONY: clean
clean:
	rm -rf *.img zImage zImage-modules debroot debroot.tar

.PHONY: distclean
distclean:
	rm -rf *.img debroot debroot.tar .config dl
