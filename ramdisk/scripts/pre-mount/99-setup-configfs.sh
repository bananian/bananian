#!/sbin/sh
mount -t configfs none /config
cfs=/config/usb_gadget
if [ ! -e $cfs ]; then
  echo "$cfs does not exist, usb networking cannot be configured" >&2
else
  echo "setting up configfs..."
  mkdir $cfs/g1
  echo 0x18D1 > $cfs/g1/idVendor # Google Inc.
  echo 0xD001 > $cfs/g1/idProduct # Nexus 4 (fastboot)
  mkdir $cfs/g1/strings/0x409 # english strings
  echo Bananian > $cfs/g1/strings/0x409/manufacturer
  echo Bananian > $cfs/g1/strings/0x409/serialnumber
  echo Debug > $cfs/g1/strings/0x409/product
  mkdir $cfs/g1/functions/rndis.usb0
  mkdir $cfs/g1/configs/c.1
  mkdir $cfs/g1/configs/c.1/strings/0x409
  echo rndis > $cfs/g1/configs/c.1/strings/0x409/configuration
  ln -s $cfs/g1/functions/rndis.usb0 $cfs/g1/configs/c.1
  echo "$(ls /sys/class/udc)" > $cfs/g1/UDC
  ifconfig usb0 up
  ifconfig usb0 10.42.0.2 netmask 10.42.0.1
  echo "configfs setup done:"
  ifconfig -a
fi
umount /config
