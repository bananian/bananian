#!/sbin/sh
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

ROOT_PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

bootstrap () {
  # Configure the base system without any interaction. Retry once if it fails.
  if DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C PATH=$ROOT_PATH chroot /root /bin/sh -c "dpkg --force-depends --configure libc6; dpkg --configure mawk; dpkg --configure base-passwd; dpkg --configure base-files; (dpkg --configure -a || dpkg --configure -a) && rm -f /UNPACKED_STAMP"; then
    /scripts/bootstrap-progress.sh success
  else
    /scripts/bootstrap-progress.sh error
  fi
}

if [ -f /root/UNPACKED_STAMP ]; then
  bootstrap | /scripts/bootstrap-progress.sh work
fi
